use std::collections::HashMap;
//extern crate textplots;
//use textplots::{Chart, Plot, Shape, utils};
use textplots::{Chart, Plot, Shape};
//use core::marker::PhantomData;
//use std::marker::PhantomData;

macro_rules! hashmap {
    ($( $key: expr => $val: expr ),*) => {{
         let mut map = ::std::collections::HashMap::new();
         $( map.insert($key, $val); )*
         map
    }}
}

#[derive(Debug)]
struct Ressource<'n> {
    name: String,
    busy: bool,
    by: Option<&'n Task>,
    time: f32,
}
impl<'n> Ressource<'n> {
    fn mod_busy(&mut self, t: Option<&'n Task>) {
        if self.busy == true {
            self.busy = false;
            self.by = None;
        }
        else {
            self.busy = true;
            self.by = t;
        }
    }
    fn mod_time(&mut self, d: f32) {
        self.time = d;
    }
    fn print_name(&self){
        println!("ressource name: {}", self.name)
    }
    // fn add_busy(&mut self, t: &'n Task){
    //     self.by=Some(t);
    // }
    // fn rm_busy(&mut self){
    //     self.by=None;
    // }
}

struct ListOfRessource {
    list : HashMap<String, (String, f32)>,
}
impl ListOfRessource {
    fn add_time (&mut self, name: &String, d: f32) {
        println!("[fn add_time] ressource: {:?} + time {}", self.list.get(name),d);
        let y = match self.list.get(name) {
            Some(y) => y.1,
            None => 0.0,
        };
        self.list.insert(String::from(name.clone()), (String::from(name.clone()), y + d));
    }
}

#[derive(Debug)]
struct Task{
    id: u32,
    duration: f32,
    rank: u32,
    order: Vec<String>,
}

fn main() {
    const MAX: f32 = 10.0;
    //let mut abs: f32 = 0.0;
    let mut a = Ressource {
        name: String::from("A"),
        busy: false,
        by: None,
        time: 0.0,
    };
    let mut b = Ressource {
        name: String::from("B"),
        busy: false,
        by: None,
        time: 0.0,
    };
    let t1 = Task {
        id: 1,
        duration: 2.0,
        rank: 1,
        order: vec![String::from("A"), String::from("B")],
    };
    let t2 = Task {
        id: 2,
        duration: 3.0,
        rank: 2,
        order: vec![String::from("B"), String::from("A")],
    };
    println!("ressources: {:#?}, {:#?}, t1: {:#?}, t2: {:#?}", a, b, t1, t2);
    a.mod_busy(Some(&t1));
    println!("ressources: {:#?}, {:#?}, t1: {:#?}, t2: {:#?}", a, b , t1, t2);
    b.mod_busy(Some(&t2));
    println!("ressources: {:#?}, {:#?}, t1: {:#?}, t2: {:#?}", a, b , t1, t2);
    b.mod_busy(None);
    a.mod_busy(None);
    println!("ressources: {:#?}, {:#?}, t1: {:#?}, t2: {:#?}", a, b , t1, t2);

    let mut ranking = vec![&t2,&t1];
    ranking.sort_unstable_by_key(|k| k.rank); //ranks tasks by priority
    println!("task ranked by priority : {:?}", ranking);

    let mut asked_order = Vec::new();
    for index in 0..ranking.len() {
        let x = ranking.get_mut(index);
        //println!("{:?}", x);
        let y = match x {
            Some(y) => &y.order,
            None => break,
        };
        //println!("{:?}", y);
        asked_order.push(y);
    };
    println!("resources asked by tasks once ranked (var asked_order) : {:?}", asked_order); // displays resources asked by tasks once ranked
    println!("(var asked_order[0][0]) {}",asked_order[0][0]);
    println!("(var asked_order[0][1]) {}",asked_order[0][1]);
    println!("(var asked_order[1][0]) {}",asked_order[1][0]);
    println!("(var asked_order[1][1]) {}",asked_order[1][1]);

    println!("\n begin loop\n");

    // let mut stack: [(&Task, f32, f32); 2] = [(&t1, 0.0, 0.0), (&t2, 0.0, 0.0)]; // stack by resource : A, B
    let mut timeof_task: [f32; 2] = [0.0,0.0];
    // let mut max_stack: [f32; 2] = [0.0, 0.0];
    // //let mut greatest_duration: f32;
    // let mut elapsed_time: f32 = 0.0;
    let tab_resource=[&a,&b];
    // //let mut min_stack = 0.0;
    let mut counter = 0;
    // //let mut max_reached = MAX;
    let mut max = 0.0;
    // let mut min = 0.0;

    // BEGIN: this loop assigns tasks to resources, sort by resource asked by task, and priority
    // while min <= MAX {
    //     for j in 0..asked_order.len() { // par ressource
    //         println!("\nNext round {} (j)",j);
    //         greatest_duration = 0.0;
    //         for i in 0..asked_order[j].len() { // par tâches
    //             // for i in 0..asked_order.len() {
    //             //println!("(var asked_order[{}] (i)) {:?}\n", i, asked_order[i]);
    //             //println!("task ranked: {} ie, {:?} asks for resource {}",j+1, ranking[j], asked_order[j][i]); // task j asks for ressource i
    //             println!("{:?} asks for resource {} (var asked_order[{}][{}] (i,j))", ranking[i], asked_order[i][j], i, j); // task j asks for ressource i
    //             for k in 0..tab_resource.len(){
    //                 println!("======== {:?} ======== (var tab_res {} (k))", tab_resource[k], k);
    //                 if asked_order[i][j] == tab_resource[k].name{
    //                     //println!("if1");
    //                     println!("/!\\ found {:?} that needs {:?}", ranking[i], tab_resource[k]);
    //                     stack[k] = (ranking[i], ranking[i].duration, stack[k].2);
    //                     //if stack[k].2 + ranking[i].duration >= abs {
    //                     //    println!("if2");
    //                     //    abs = stack[k].2 + ranking[i].duration;
    //                         //elapsed_time = greatest_duration - elapsed_time;
    //                         if stack[k].2 + ranking[i].duration > MAX {
    //                             max_stack[k] = stack[k].2 + ranking[i].duration;
    //                             println!("/!\\/!\\/!\\ MAX reached for {:?} : {} /!\\/!\\/!\\", stack[k], max_stack[k]);
    //                             break;
    //                         }
    //                         else {
    //                             // if stack[k].2 + ranking[i].duration <= min_stack {
    //                             //     min_stack = stack[k].2 + ranking[i].duration;
    //                             //     println!("\nbruuuuuh\n");
    //                             // }
    //                             //println!("else");
    //                             println!("current time: task {} is at time {}", ranking[i].id, timeof_task[i]);
    //                             //println!("{:?}",stack[k]);
    //                             println!("current time of resource {} is {}", tab_resource[k].name, stack[k].2);
    //                             println!("-----> max between both previous time is {}", timeof_task[i].max(stack[k].2));
    //                             //stack[k] = (ranking[i], ranking[i].duration, stack[k].2 + ranking[i].duration);
    //                             stack[k] = (ranking[i], ranking[i].duration, timeof_task[i].max(stack[k].2) + ranking[i].duration);
    //                             if elapsed_time < stack[k].2 {
    //                                 elapsed_time = stack[k].2;
    //                             }
    //                             //println!("attributed: {:?} : {:?}", tab_resource[k], stack[k]);
    //                             println!("after a duration of {}, time of resource {} is {}", ranking[i].duration, tab_resource[k].name, stack[k].2);
    //                             timeof_task[i] = timeof_task[i] + ranking[i].duration;
    //                             println!("current time: task {} is at time {}", ranking[i].id, timeof_task[i]);
    //                             counter = counter + 1;
    //                         }
    //
    //                     //}
    //
    //                     // add abs for each resource
    //
    //                     // stack[k] = (stack[k].0, stack[k].1, stack[k].2 + greatest_duration);
    //                 }
    //                 // if asked_order[j][i] == tab_res[k].name{
    //                 //     println!("/!\\ found {:?} that need {:?}", ranking[j], tab_res[k]);
    //                 //     stack[i] = (ranking[j], ranking[j].duration, stack[i].2);
    //                 //     if ranking[j].duration > greatest_duration {
    //                 //         greatest_duration = ranking[j].duration;
    //                 //     }
    //                 //     stack[i] = (stack[i].0, stack[i].1, stack[i].2 + greatest_duration);
    //                     //println!("stack_{}: {:?}, duration: {}", asked_order[j][i], stack[i], greatest_duration);
    //                 //}
    //             }
    //             //if abs > MAX {
    //                 //println!("/!\\/!\\/!\\ MAX reached /!\\/!\\/!\\");
    //             //    break;
    //             //}
    //         }
    //         // if min_stack > MAX {
    //         //     println!("/!\\/!\\/!\\ MAX reached : min_stack = {}/!\\/!\\/!\\", min_stack);
    //         //     break;
    //         // }
    //         //abs = abs + elapsed_time;
    //         //abs = greatest_duration;
    //         println!("abs time {:?}", elapsed_time);
    //     }
    //     for k in 0..tab_resource.len(){
    //         if max_stack[k] >= max {
    //             max = max_stack[k];
    //         }
    //         println!("max_stack[{}] : {}", k,  max_stack[k]);
    //     }
    //     println!("max {}", max);
    //     min = max;
    //     for k in 0..tab_resource.len(){
    //         if max_stack[k] <= max && max_stack[k] > MAX && max_stack[k] <= min {
    //             min = max_stack[k];
    //         }
    //         //println!("max_stack[{}] : {}", k,  max_stack[k]);
    //     }
    //     println!("min {}", min);
    //     if min > MAX {
    //         println!("/!\\/!\\/!\\ MAX reached /!\\/!\\/!\\");
    //         break;
    //     }
    //
    //     println!("\nall tasks terminated: next iteration")
    // }
    // println!("\n end loop");
    // println!("{} tasks runned in {} units of time \n", counter, elapsed_time);
    // END: this loop assigns tasks to resources, sort by resource asked by task, and priority


    // BEGIN: this loop searches for a given resource
    // for rs in tab.iter(){
    //     println!("{:?}",rs.name.as_str());
    //     if rs.name.as_str() == "A" {
    //         println!("resource searched: {:?}",rs);
    //     }
            // let y = match rs.name.as_str() { // prints the name of all resources
            //      "A" => String::from("A"),
            //      "B" => String::from("B"),
            //      _ => String::from("empty"),
            // };
            //println!("task: {}",y);
    //}
    // END: this loop searches for a given resource
    println!("looking for resource B using fn return_resource_from_name: {:?}", return_resource_from_name (tab_resource, String::from("B")));



{
        let mut map_res : HashMap<String, &Ressource> = HashMap::new();
map_res.insert("A".to_string(), & a);
map_res.insert("B".to_string(), & b);
//println!("{:?}", map_res.find_equiv(&"A"));
//println!("{:?}", map_res.find_equiv(&"B"));
println!("{:?}", map_res.get_mut("A"));

println!("{:?}", map_res.get_mut("A"));
println!("{:?}", map_res.get_mut("B"));
println!("{:?}", map_res.get_mut("B"));
}
//a.mod_busy(Some(&t1));

//{
    let mut map_res : HashMap<String, & Ressource> = HashMap::new();
 map_res.insert("A".to_string(), & a);
 map_res.insert("B".to_string(), & b);

println!("{:?}", map_res.get("A"));
//}

println!("\n\n nouvelle partie \n\n");

let mut i = 1;
//let mut j = 0;
//timeof_task = [0.0, 0.0];
let mut ma_liste = ListOfRessource {
    list: hashmap![ String::from("A") => (String::from("A"), 0.0), String::from("B") => (String::from("B"), 0.0)],
};
//let mut taches: [(&Task, f32); 2] = [(&t1, 0.0), (&t2, 0.0)];
//let mut ressources: [(&Ressource, f32); 2] = [(&a, 0.0), (&b, 0.0)];
let mut next_task: [Vec<String>; 2] = [t1.order.iter().rev().cloned().collect(), t2.order.iter().rev().cloned().collect()];
let mut next: String = String::from("empty next");
let mut smallest_task: f32;
let mut smallest_task_id = 0;
let mut plot_a: Vec<(f32, f32)> = vec!((0.0, 0.0));
let mut plot_b: Vec<(f32, f32)> = vec!((0.0, 0.0));

while max <= MAX {
    println!("\n new round {:?}\n", next_task);

    while i < asked_order.len()+1 {
    println!("----> task {}", i-1);
    //while j < asked_order[i-1].len(){
        //println!("{:?}", map_res.get(&asked_order[i][j]));
        //println!("{:?}, {}", ma_liste.list.get(&asked_order[i-1][j]), &asked_order[i-1][j]);
        // next = match next_task[i-1].first_mut() {
        //     Some(y) => y.to_string(),
        //     None => String::from("empty"),
        // };

        if let Some(first) = next_task[i-1].pop() {
            next = first.to_string()
        };
        println!("{:?}, {}", ma_liste.list.get(&next), next);

        println!("====== next resource is {} and next_task[{}] is {:?}", next, i-1, next_task[i-1]);
        println!("{:?}", next_task);
        let y = match ma_liste.list.get(&next) {
            Some(y) => y.1,
            None => 0.0,
        };
        println!("time of task is {}", timeof_task[i-1]);
        if timeof_task[i-1] <= y && y + ranking[i-1].duration <= MAX {
            println!("affectation {} <= {}: {:?}", timeof_task[i-1], y, ranking[i-1]);
            counter = counter + 1;
            ma_liste.add_time(&next, ranking[i-1].duration);
            timeof_task[i-1] = y + ranking[i-1].duration;
            println!("{:?}", ma_liste.list.get(&next));
            if next_task[i-1].is_empty() {
                match i {
                    1 => next_task[i-1] = t1.order.iter().rev().cloned().collect(),
                    2 => next_task[i-1] = t2.order.iter().rev().cloned().collect(),
                    _ => next_task[i-1] = vec![String::from("empty stack"), String::from("empty stack")],
                };
            }
            if next == String::from("A"){
                if i-1 == 0 {
                    plot_a.push((timeof_task[i-1], 2.0));
                }
                else if i-1 == 1 {
                    plot_a.push((timeof_task[i-1], 1.0));
                }
            }
            else if next == String::from("B"){
                if i-1 == 0 {
                    plot_b.push((timeof_task[i-1], 2.0));
                }
                else if i-1 == 1 {
                    plot_b.push((timeof_task[i-1], 1.0));
                }
            }
            i = 0;
        }
        else if y + ranking[i-1].duration > MAX {
            println!("pas d'affectation: {} > {}",y + ranking[i-1].duration, MAX);
            next_task[i-1].push(next.clone());
        }
        else {
            println!("pas d'affectation: {} > {}", timeof_task[i-1], y);
            next_task[i-1].push(next.clone());
        }
        //    j = j + 1;
        //}
        i = i + 1;
        //j = 0;
    }
    println!("\n end round\n");
    smallest_task = timeof_task[0];
    for j in 0..timeof_task.len() {
         if timeof_task[j] < smallest_task {
             smallest_task = timeof_task[j];
             smallest_task_id = j;
         }
    }
    println!("smallest time of task {} for task {}", timeof_task[smallest_task_id], smallest_task_id+1);

    if let Some(first) = next_task[smallest_task_id].pop() {
        next = first.to_string()
    };
    println!("{:?}, {}", ma_liste.list.get(&next), next);

    println!("====== next resource is {} and next_task[{}] is {:?}", next, smallest_task_id, next_task[smallest_task_id]);
    let y = match ma_liste.list.get(&next) {
        Some(y) => y.1,
        None => 0.0,
    };
    println!("time of task is {}", timeof_task[smallest_task_id]);
    //if timeof_task[smallest_task_id] + ranking[smallest_task_id].duration <= MAX{
    if timeof_task[smallest_task_id] + ranking[smallest_task_id].duration <= MAX && y + ranking[smallest_task_id].duration <= MAX {
        if next == String::from("A"){
            if smallest_task_id == 0 {
                plot_a.push((y, 0.0));
                plot_a.push((timeof_task[smallest_task_id], 0.0));
            }
            else if smallest_task_id == 1 {
                plot_a.push((y, 0.0));
                plot_a.push((timeof_task[smallest_task_id], 0.0));
            }
        }
        else if next == String::from("B"){
            if smallest_task_id == 0 {
                plot_b.push((y, 0.0));
                plot_b.push((timeof_task[smallest_task_id], 0.0));

            }
            else if smallest_task_id == 1 {
                plot_b.push((y, 0.0));
                plot_b.push((timeof_task[smallest_task_id], 0.0));
            }
        }
        println!("affectation at time {}: {:?}", timeof_task[smallest_task_id], ranking[smallest_task_id]);
        counter = counter + 1;
        ma_liste.add_time(&next, ranking[smallest_task_id].duration+timeof_task[smallest_task_id]-y);
        println!("=> time of task is {}, y is {}, duration is {}", timeof_task[smallest_task_id], y, ranking[smallest_task_id].duration);
        timeof_task[smallest_task_id] = timeof_task[smallest_task_id] + ranking[smallest_task_id].duration;
        println!("{:?}", ma_liste.list.get(&next));
        if next_task[smallest_task_id].is_empty() {
            match smallest_task_id {
                0 => next_task[smallest_task_id] = t1.order.iter().rev().cloned().collect(),
                1 => next_task[smallest_task_id] = t2.order.iter().rev().cloned().collect(),
                _ => next_task[smallest_task_id] = vec![String::from("empty stack"), String::from("empty stack")],
            };
        }
        println!("after update, time of task is {}", timeof_task[smallest_task_id]);
        if next == String::from("A"){
            if smallest_task_id == 0 {
                plot_a.push((timeof_task[smallest_task_id], 2.0));
            }
            else if smallest_task_id == 1 {
                plot_a.push((timeof_task[smallest_task_id], 1.0));
            }
        }
        else if next == String::from("B"){
            if smallest_task_id == 0 {
                plot_b.push((timeof_task[smallest_task_id], 2.0));
            }
            else if smallest_task_id == 1 {
                plot_b.push((timeof_task[smallest_task_id], 1.0));
            }
        }
    }
    else {
        println!("plus d'affectation possible: {}+{} or {}+{} > {}", timeof_task[smallest_task_id], ranking[smallest_task_id].duration, y, ranking[smallest_task_id].duration, MAX);
        max = timeof_task[0];
        for j in 0..timeof_task.len() {
             if timeof_task[j] > max {
                 max = timeof_task[j];
             }
        }
        println!("\nlargest time of task {}, {} tasks runned in less than {} units of time\n", max, counter, MAX);
        break;
    }

//    max = max + 1.0;
    i = 1;
    //j = 0;
}

println!("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n%%%%%%%%%%%%%%%%%% PLOTTING with t1=(.,2), t2=(.,1) %%%%%%%%%%%%%%%%%%\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
println!("{:?}", plot_a);
println!("{:?}", plot_b);

plot_fig(plot_a, plot_b);

}

fn return_resource_from_name<'n> (a: [&'n Ressource; 2], name: String) -> Option< &'n Ressource<'n>> {
    for rs in a.iter(){
        //println!("resource checked by fn: {:?}",rs);
        if rs.name.as_str() == name {
            //println!("resource found: {:?}",rs);
            return Some(rs);
        }
    }
    return None;
}

fn plot_fig(plot_a: Vec<(f32,f32)>, plot_b: Vec<(f32,f32)>) {
    //let points = [(0.0, 11.677842), (1.0, 9.36832), (2.0, 9.44862), (3.0, 8.933405), (4.0, 11.812017), (5.0, 9.225881), (6.0, 11.498754), (7.0, 8.048191), (8.0, 11.141933), (9.0, 8.908442), (10.0, 9.795515), (11.0, 9.115709), (12.0, 8.907767), (13.0, 11.153717), (14.0, 10.893343), (15.0, 9.299543), (16.0, 9.701611), (17.0, 10.5055685), (18.0, 9.29626), (19.0, 9.14646), (20.0, 8.892315), (21.0, 10.202528), (22.0, 9.776375), (23.0, 9.106522), (24.0, 10.017476), (25.0, 9.344163), (26.0, 10.087107), (27.0, 10.595626), (28.0, 10.26935), (29.0, 9.619124), (30.0, 9.759572), (31.0, 10.563857), (32.0, 10.254505), (33.0, 10.876704), (34.0, 10.487561), (35.0, 9.672023), (36.0, 11.139632), (37.0, 11.562245), (38.0, 12.311694), (39.0, 8.636497), (40.0, 10.127292), (41.0, 10.867212), (42.0, 10.285108), (43.0, 8.08108), (44.0, 10.198147), (45.0, 10.746926), (46.0, 9.746353), (47.0, 9.767584), (48.0, 10.165949), (49.0, 10.08595), (50.0, 10.08878), (51.0, 11.137325), (52.0, 9.596683), (53.0, 9.3425045), (54.0, 9.627313), (55.0, 10.010254), (56.0, 9.526915), (57.0, 11.239794), (58.0, 8.913055), (59.0, 10.136724), (60.0, 10.72442), (61.0, 9.044553), (62.0, 8.657727), (63.0, 10.284623), (64.0, 10.32074), (65.0, 8.713137), (66.0, 9.928085), (67.0, 8.439049), (68.0, 9.942111), (69.0, 9.212654), (70.0, 8.224474), (71.0, 11.252406), (72.0, 8.816701), (73.0, 10.853656), (74.0, 8.788404), (75.0, 10.526451), (76.0, 10.779287), (77.0, 9.357046), (78.0, 10.788815), (79.0, 10.013228), (80.0, 10.859512), (81.0, 10.734754), (82.0, 10.504648), (83.0, 10.104772), (84.0, 10.20058), (85.0, 10.663727), (86.0, 11.040146), (87.0, 12.313308), (88.0, 10.41382), (89.0, 9.867012), (90.0, 9.984057), (91.0, 8.8879595), (92.0, 9.459296), (93.0, 9.00407), (94.0, 10.469272), (95.0, 9.79327), (96.0, 12.317585), (97.0, 8.190812), (98.0, 12.709852), (99.0, 9.720502)];

    // println!("\ny = some points");
    // Chart::new(180, 60, 0.0, 100.0)
    //     .lineplot( Shape::Lines(&points) )
    //     .display();
    //

    // println!("\ny = as steps");
    // Chart::new(180, 60, 0.0, 100.0)
    //     .lineplot( Shape::Steps(&points) )
    //     .display();

    // println!("\nressource A");
    // Chart::new(180, 32, 0.0, 10.0)
    //     .lineplot( Shape::Steps(&plot_a) )
    //     .display();
    //
    // println!("\nressource A");
    // Chart::new(180, 32, 0.0, 10.0)
    //     .lineplot( Shape::Steps(&plot_b) )
    //     .display();


    println!("\nressource A");
    Chart::new(180, 32, 0.0, 10.0)
        .lineplot( Shape::Bars(&plot_a) )
        .display();

    println!("\nressource B");
    Chart::new(180, 32, 0.0, 10.0)
        .lineplot( Shape::Bars(&plot_b) )
        .display();
    // let hist = utils::histogram(&points, 6.0, 15.0, 16);
    // println!("\ny = and finally as nice histogram bars");
    // Chart::new(180, 60, 6.0, 14.0)
    //     .lineplot( Shape::Bars(&hist) )
    //     .nice();
}
