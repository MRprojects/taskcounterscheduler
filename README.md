## weighted task scheduler

I developped an algorithm to schedule weighted tasks on a single-core CPU in Rust.
Depending on the weight (preemption) of a task the scheduler tries to optimize the execution of tasks

*March 2019*
